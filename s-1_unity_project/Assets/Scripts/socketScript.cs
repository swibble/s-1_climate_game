﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;


public class socketScript : MonoBehaviour {
	
	//variables for networking calls
	private TCPConnection myTCP;
	private string serverMsg;
	public string msgToServer;

	//variables for input
	public string location;
	public string temperature;
	public string date;

	//variables for gameObjects
	public Text location_UIText;
	public Text temperature_UIText;
	public Text date_UIText;


	void Awake() {
		//add a copy of TCPConnection to this game object
		myTCP = gameObject.AddComponent<TCPConnection>();	
	}

	void Start () {
		location_UIText.text = "";
		temperature_UIText.text = "";
		date_UIText.text = "";
	}

	void Update () {

		//keep checking the server for messages, if a message is received from server, it gets logged in the Debug console (see function below)
		SocketResponse ();
	}

	void OnGUI() {

		//if connection has not been made, display button to connect
		if (myTCP.socketReady == false) {
			if (GUILayout.Button ("Connect")) {
				
				//try to connect
				Debug.Log("Attempting to connect..");
				myTCP.setupSocket();	
			}
		}

		//once connection has been made, display editable text field with a button to send that string to the server (see function below)
		
		if (myTCP.socketReady == true) {
			msgToServer = GUILayout.TextField(msgToServer);
			if (GUILayout.Button ("Write to server", GUILayout.Height(30))) {
				SendToServer(msgToServer);	
			}
		}	
	}

	//socket reading script
	
	void SocketResponse() {

		string serverSays = myTCP.readSocket();
		if (serverSays != "") {
//			Dictionary<string, string> N = Serializer.DeSerialize(serverSays);
			JSONObject N = new JSONObject (serverSays);
			location = N ["loc"].str;
			temperature = "" + N ["temp"];
			date = N ["date"].str;
			location_UIText.text = location;
			temperature_UIText.text = temperature;
			date_UIText.text = date;
			Debug.Log ("Location is: " + location);
			Debug.Log ("Temperature is: " + temperature);
			Debug.Log("Date is: " + date);

			if (location == "seattle") {
				SendToServer ("this is seattle");
			}
		}
	}

	//send message to the server
	
	public void SendToServer(string str) {
		
		myTCP.writeSocket(str);
		
		Debug.Log ("[CLIENT] -> " + str);
		
	}
}

