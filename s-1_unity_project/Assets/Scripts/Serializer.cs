﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Serializer{
	private static Dictionary<string, string> dictionaryFromJsonStrStr = new Dictionary<string, string> ();

	public static Dictionary<string, string> DeSerialize(string Json1)
	{
		JSONObject jsonCharInfo = new JSONObject (Json1);
		int numOfListsInJson = jsonCharInfo.list.Count;
		for (int k=0; k<numOfListsInJson; k++  ) {
			for (int i=0; i<jsonCharInfo.list[k].keys.Count; i++  ) {
				string key = (string)jsonCharInfo.list[k].keys [i];
				JSONObject value = (JSONObject)jsonCharInfo.list[k].list [i];
				if(!dictionaryFromJsonStrStr.ContainsKey(key))
				{
					dictionaryFromJsonStrStr [key] = value.str;
				}
			}
		}
		return dictionaryFromJsonStrStr;
	}
}