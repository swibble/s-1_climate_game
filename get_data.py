import sqlite3
import time
from noaa_api.noaa_api import NoaaApi


def get_data(n):
    n.params['offset'] += 1
    print('offset is {}'.format(n.params['offset']))
    n.get()
    n.values()
    if len(n.results) < 1:
        raise Exception('all done')
    else:
        locs = [(None, x['datacoverage'],
                x['mindate'], x['maxdate'],
                x['id'], x['name']) for x in n.results]
        return locs


def main():
    try:
        conn = sqlite3.connect('locations.sqlite')
        c = conn.cursor()
        n = NoaaApi()
        n.endpoint = 'locations'
        n.params = {'datasetid': 'GHCND',
                 'enddate': '2016-04-18',
                 'limit': 1000,
                 'locationcategoryid': 'CITY',
                 'offset': 1351,
                 'startdate': '2016-01-01'}
        for i in range(1000):
            locs = get_data(n)
            c.executemany("INSERT INTO CITY VALUES \
                (?,?,?,?,?,?)", locs)
            conn.commit()
            time.sleep(.2)

    except KeyboardInterrupt:
        print('offset is {}'.format(n.params['offset']))
        conn.commit()
        conn.close()



if __name__ == '__main__':
    main()