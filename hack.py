import socket
import time


sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setblocking(0)
sock.bind( ("127.0.0.1", 7000) )

# data = sock.recv(1024)
# print(data)

while True:
	try:
		print(sock.recv(1024))
	except socket.error as e:
		print(str(e))
		if str(e).startswith('[Errno 35]'):

			time.sleep(0.1)
	except KeyboardInterrupt:
		sock.close()
		break

