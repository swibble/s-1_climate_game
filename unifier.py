## Author:  Luke Caldwell
## Org: Duke University Speculative Sensation Lab
## Website: http://s-1lab.org
## License: Creative Commons BY-NC-SA 3.0
##          http://creativecommons.org/licenses/by-nc-sa/3.0/

import json
import socket
import random
import time
from noaa_api.noaa_api import NoaaApi


def dummy_data():
    data = {'loc': "Zambia", 'temp': random.randint(10, 1000)}
    return data


def get_data(noaa):
    location = "CITY:AE000001"
    noaa.loc(location)
    noaa.startdate("2016-01-01")
    noaa.limit(2)
    noaa.data_type('TMAX')
    noaa.get()
    noaa.values()
    results = noaa.results
    temp = results[0]['value'] / 10.0
    ret = {'location': location, 'temp': temp}
    print(ret)
    return ret


def main(noaa):
    CONNECTION = ("127.0.0.1", 9997)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # s.connect(CONNECTION)

    while True:
        try:
            # data = json.dumps(get_data(noaa))
            data = b"foo"
            s.sendto(data, CONNECTION)
            time.sleep(1)
        except KeyboardInterrupt:
            print('closing connection...')
            s.close()
            break
        except Exception:
        	pass


if __name__ == '__main__':
    # noaa = NoaaApi()
    # get_data(noaa)
    noaa = 1
    main(noaa)
