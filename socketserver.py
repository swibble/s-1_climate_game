#!/usr/bin/python3

import socket
import random
import time
import json

locations = ['seattle', 'durham', 'atlanta']

host = "127.0.0.1"
port = 9993

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))
# s.setblocking(0)

s.listen(1)

conn, addr = s.accept()
print('connection', addr)
conn.setblocking(0)
time.sleep(3)

timer = time.time()

while True:

    try:
        # r, _, _ = select.select([conn], [], [])
        # if r:
        try:
            incoming = conn.recv(512)
        except socket.error:
            incoming = False
            # print('past incoming...')
        if incoming:
            print(incoming)

        if (time.time() - timer) > 3:
            output = json.dumps({
                'loc': random.choice(locations),
                'temp': random.randint(0, 100),
                'date': "{}-{}-{}".format(random.randint(1950, 2017), random.randint(1, 12), random.randint(1, 31))
                })
            print('sending data...{}'.format(output))
            conn.send(output)
            timer = time.time()

    except KeyboardInterrupt:
        break
    except Exception as e:
        print(str(e))
        raise(e)
    

s.close()
# b.close()
