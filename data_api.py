import requests
import json

class WorldBankAPI():

	_type = None
	var = None
	start = None
	end = None
	loc = None
	def get(self):
		endpoint = 'http://climatedataapi.worldbank.org/climateweb/rest/v1/country/'
		self.endpoint = endpoint + '{}/{}/{}/{}/{}'.format(
			self._type, self.var, self.start, self.end, self.loc)

		r = requests.get(self.endpoint)
		self.response = r.json()



w = WorldBankAPI()
w._type = 'mavg'
w.var = 'pr'
w.start = '1980'
w.end = '1999'
w.loc = 'AFG'

w.get()
print(w.response)