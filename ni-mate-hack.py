
from animation_delicode_ni_mate_tools import NImateReceiver
from pythonosc import osc_message_builder
from pythonosc import udp_client
import random
import time

rec = NImateReceiver(7001, 7001)
client = udp_client.UDPClient('127.0.0.1', 7000)
timer = time.time()
while True:
	data = rec.run()
	if data:
		print(data)
		msg = osc_message_builder.OscMessageBuilder(address=data[0].decode())
		for n, i in enumerate(data[2:]):
			if n == 1:
				r = 0
			# n = random.randint(1, 4)
			elif random.randint(-6, 6) % 2 == 0:
			 # and (timer - time.time()) > .1:
				r = random.random()/6
				timer = time.time()
				# rand = random.randint(3, 9)
				# r = 1 + rand
			else:
				r = 0
			msg.add_arg(i + r)
		output = msg.build()
		client.send(output)
